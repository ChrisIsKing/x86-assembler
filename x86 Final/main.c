//
//  main.c
//  x86 Final
//
//  Created by Christopher Clarke on 4/4/16.
//  Copyright © 2016 Assembly Language. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "x86_interpreter.h"



int main(int argc, const char * argv[]) {
    if (argc < 2) {
        printf("Usage: ./main example.txt");
        exit(1);
    }
    
    FILE* x86_code = fopen(argv[1], "r");
    if (x86_code == NULL) {
        printf("No such file found");
        exit(2);
    }
    
    FILE* mips_hex = fopen("mips_hex.txt", "w+");
    if (mips_hex == NULL) {
        printf("Unable to create output file");
        exit(3);
    }
    
    char buffer[100];
    x86_instruction* x86_instruction = malloc(sizeof(x86_instruction));
    mips_instruction* mips_instruction = malloc(sizeof(mips_instruction));
    while (fgets(buffer, 100, x86_code)) {
        x86_instruction = decode_instruction(buffer);
        if (x86_instruction->instruction_type == 1) {
            load_symbol_table(x86_instruction);
        }
        if (x86_instruction->instruction_type == 2) {
            while (true) {
                fgets(buffer, 100 , x86_code);
                char* line = strstr(buffer, ".CODE");
                if (line != NULL) {
                    break;
                } else {
                    load_data_segment(decode_data_segment(buffer));
                }
            }
        }
    }
    rewind(x86_code);
    while (fgets(buffer, 100, x86_code)) {
        x86_instruction = decode_instruction(buffer);
        if (x86_instruction->instruction_type == 2) {
            while (true) {
                fgets(buffer, 100 , x86_code);
                char* line = strstr(buffer, ".CODE");
                if (line != NULL) {
                    break;
                } else {
                    ;
                }
            }
        }
        load_mips_instruction(x86_instruction, mips_hex);
    }
}
