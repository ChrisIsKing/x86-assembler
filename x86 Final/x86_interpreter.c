//
//  x86_interpreter.c
//  x86 Final
//
//  Created by Christopher Clarke on 4/4/16.
//  Copyright © 2016 Assembly Language. All rights reserved.
//

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "x86_interpreter.h"

symbol* symbol_table[100];
data* data_table[100];

int line_count = 0;
int offset = 0;

x86_instruction* decode_instruction(char* input) {
    x86_instruction* instruction = malloc(sizeof(x86_instruction));
    char* string;
    string = strstr(input, ".DATA");
    if (string != NULL) {
        instruction->instruction_type = 2;
        string = strtok(input, " ");
        strcpy(instruction->label, string);
        return instruction;
    }
    string = strchr(input, ':');
    if (string != NULL) {
        string = strtok(input, ": ");
        strcpy(instruction->label, string);
        instruction->instruction_type = 1;
        return instruction;
    }
    line_count++;
    instruction->instruction_type = 0;
    char* tmp = strstr(input, "[");
    if (tmp != NULL) {
        instruction->instruction_type = 3;
        string = strtok(input, " ");
        strcpy(instruction->instruction, string);
        string = strtok(NULL, ",");
        if (string != NULL) {
            strcpy(instruction->operand1, string);
            string = strtok(NULL, "]");
            if (string != NULL) {
                strcat(string, "]");
                strcpy(instruction->operand2, string);
                string = strtok(NULL, ", ");
                if (string != NULL) {
                    strcpy(instruction->operand3, string);
                    return instruction;
                }
            }
            else
                return instruction;
        }
    }
    else {
        string = strtok(input, " ");
        strcpy(instruction->instruction, string);
        string = strtok(NULL, ",");
        if (string != NULL) {
            strcpy(instruction->operand1, string);
            string = strtok(NULL, ", ");
            if (string != NULL) {
                strcpy(instruction->operand2, string);
                string = strtok(NULL, ", ");
                if (string != NULL) {
                    strcpy(instruction->operand3, string);
                    return instruction;
                }
            }
            else
                return instruction;
        }
    }
    return instruction;
}

data_instruction* decode_data_segment(char* input) {
    data_instruction* instruction = malloc(sizeof(data_instruction));
    char* string;
    string = strstr(input, "TIMES");
    if (string == NULL) {
        string = strtok(input, " ");
        strcpy(instruction->variable_name, string);
        string = strtok(NULL, " ");
        if (string == NULL) {
            return NULL;
        }
        strcpy(instruction->mem_size, string);
        string = strtok(NULL, "");
        char* tmp = strstr(string, "\'");
        if (tmp == NULL) {
            strcpy(instruction->initial_value, string);
            instruction->instruction_type = 0;
            return instruction;
        }
        else {
            char* tmp = string;
            tmp = tmp + 1;
            char* tmp2 = strstr(tmp,"\'");
            int len = strlen(tmp2);
            strncpy(instruction->initial_value, tmp, strlen(tmp)-len);
            instruction->amount = strlen(instruction->initial_value);
            instruction->instruction_type = 1;
            return instruction;
        }
    }
    string = strtok(input, " ");
    strcpy(instruction->variable_name, string);
    string = strtok(NULL, " ");
    if (string == NULL) {
        return NULL;
    }
    strcpy(instruction->keyword, string);
    string = strtok(NULL, " ");
    instruction->amount = atoi(string);
    string = strtok(NULL, " ");
    strcpy(instruction->mem_size, string);
    string = strtok(NULL, " ");
    strcpy(instruction->initial_value, string);
    instruction->instruction_type = 1;
    return instruction;
}

void load_data_segment(data_instruction* instruction) {
    if (instruction == NULL) {
        return;
    }
    data* new_variable = malloc(sizeof(data));
    if (new_variable == NULL) {
        printf("Error loading variable %s", instruction->variable_name);
        exit(6);
    }
    strcpy(new_variable->name, instruction->variable_name);
    int size = 0;
    if (instruction->instruction_type == 0) {
        size = get_mem_size(instruction->mem_size);
        if (offset == 0) {
            new_variable->offset = offset;
            offset += size;
            load_data_symbol(new_variable);
        } else {
            new_variable->offset = offset;
            offset += size;
            load_data_symbol(new_variable);
        }
    }
    else {
        size = get_mem_size(instruction->mem_size) * instruction->amount;
        if (offset == 0) {
            new_variable->offset = offset;
            offset += size;
            load_data_symbol(new_variable);
        }
        else {
            new_variable->offset = offset;
            offset += size;
            load_data_symbol(new_variable);
        }
    }
}

void load_data_symbol(data* node) {
    int index = hashfunction(node->name);
    if (data_table[index] == NULL) {
        data_table[index] = node;
    }
    else {
        node->next = data_table[index];
        data_table[index] = node;
    }
}

int get_mem_size(char* string) {
    if (strcmp(string, "DB") == 0) {
        return 1;
    }
    if (strcmp(string, "DW") == 0) {
        return 2;
    }
    if (strcmp(string, "DD") == 0) {
        return 4;
    }
    if (strcmp(string, "DQ") == 0) {
        return 1;
    }
    else
        return 0;
}

char* itoa(int num)
{
    /* log10(num) gives the number of digits; + 1 for the null terminator */
    int size = log10(num) + 1;
    char *x = malloc(size);
    snprintf(x, size, "%d", num);
    return x;
}

void load_symbol_table(x86_instruction* instruction) {
    load_symbol(instruction->label);
}

int hashfunction(char* word)
{
    unsigned int hashval = 0;
    for(int i = 0, len = strlen(word); i < len; i++)
    {
        hashval = (127 * hashval + word[i]) % 16908799;
    }
    return (((17 * hashval) + 19) % 100057) % 100;
}

void load_symbol(char* label) {
    int index = hashfunction(label);
    symbol* new_symbol = malloc(sizeof(symbol));
    if (new_symbol == NULL) {
        printf("Symbol Loading Error: Line %d", line_count);
        exit(5);
    }
    strcpy(new_symbol->name, label);
    new_symbol->mem_address = line_count + 1;
    if (symbol_table[index] == NULL) {
        symbol_table[index] = new_symbol;
    } else {
        new_symbol->next = symbol_table[index];
        symbol_table[index] = new_symbol;
    }
}


int get_label(char* operand) {
    char* string = strstr(operand, "[");
    if (string == NULL) {
        printf("Unknown operand @ Line %i", line_count);
        exit(4);
    }
    string = string + 1;
    char* string2 = strstr(string, "+");
    if (string2 == NULL) {
        string2 = strstr(string, "-");
        if (string2 == NULL) {
            string2 = strstr(string, "]");
            if (string2 == NULL) {
                printf("Unknown operand @ Line %i", line_count);
                exit(4);
            }
        } else {
            char data[20];
            strncpy(data, string, strlen(string) - strlen(string2));
            int address = get_label_address(data);
            if (address == -1) {
                address = get_register(data);
                if (address == -1) {
                    printf("Unknown operand @ Line %i", line_count);
                    exit(4);
                }
                return address;
            }
            string2 = strtok(string2, "]");
            return address - atoi(string2);
        }
    } else {
        char data[20];
        strncpy(data, string, strlen(string) - strlen(string2));
        int address = get_label_address(data);
        if (address == -1) {
            address = get_register(data);
            if (address == -1) {
                printf("Unknown operand @ Line %i", line_count);
                exit(4);
            }
            return address;
        }
        string2 = strtok(string2, "]");
        return address + atoi(string2);
    }
    return -1;
}

int get_label_address(char* input) {
    int hashval = hashfunction(input);
    if (symbol_table[hashval] == NULL) {
        return -1;
    }
    symbol* ptr = symbol_table[hashval];
    while (ptr != NULL) {
        if (strcmp(symbol_table[hashval]->name, input) == 0) {
            return symbol_table[hashval]->mem_address;
        }
        else {
            ptr = ptr->next;
        }
    }
    return get_label(input);
}

int get_register(char* operand) {
    if (strcmp(operand, "eax") == 0) {
        return 0x8;
    }
    if (strcmp(operand, "ebx") == 0) {
        return 0x9;
    }
    if (strcmp(operand, "ecx") == 0) {
        return 0xa;
    }
    if (strcmp(operand, "edx") == 0) {
        return 0xb;
    }
    if (strcmp(operand, "esi") == 0) {
        return 0xc;
    }
    if (strcmp(operand, "edi") == 0) {
        return 0xd;
    }
    if (strcmp(operand, "esp") == 0) {
        return 0x1D;
    }
    if (strcmp(operand, "ebp") == 0) {
        return 0xf;
    }
    if (strcmp(operand, "s0") == 0) {
        return TMP_REGISTER;
    }
    if (strcmp(operand, "s1") == 0) {
        return TMP_REGISTER_2;
    }
    if (strcmp(operand, "s2") == 0) {
        return LESS_THAN_FLAG;
    }
    if (strcmp(operand, "s3") == 0) {
        return EQUAL_FLAG;
    }
    if (strcmp(operand, "s4") == 0) {
        return GREATER_THAN_FLAG;
    }
    if (strcmp(operand, "s5") == 0) {
        return LESS_THAN_EQUAL_FLAG;
    }
    if (strcmp(operand, "s6") == 0) {
        return GREATER_THAN_EQUAL_FLAG;
    }
    if (strcmp(operand, "s7") == 0) {
        return NOT_EQUAL_FLAG;
    }
    if (strcmp(operand, "v0") == 0) {
        return 0x2;
    }
    if (strcmp(operand, "a0") == 0) {
        return 0x4;
    }
    if (strcmp(operand, "zero") == 0) {
        return 0x0;
    }
    if (strcmp(operand, "ax") == 0) {
        return 0x8;
    }
    if (strcmp(operand, "ah") == 0) {
        return 0x8;
    }
    if (strcmp(operand, "al") == 0) {
        return 0x8;
    }
    if (strcmp(operand, "bx") == 0) {
        return 0x9;
    }
    if (strcmp(operand, "bh") == 0) {
        return 0x9;
    }
    if (strcmp(operand, "bl") == 0) {
        return 0x9;
    }
    if (strcmp(operand, "cx") == 0) {
        return 0xa;
    }
    if (strcmp(operand, "ch") == 0) {
        return 0xa;
    }
    if (strcmp(operand, "cl") == 0) {
        return 0xa;
    }
    if (strcmp(operand, "dx") == 0) {
        return 0xb;
    }
    if (strcmp(operand, "dh") == 0) {
        return 0xb;
    }
    if (strcmp(operand, "dl") == 0) {
        return 0xb;
    }
    if (strcmp(operand, "si") == 0) {
        return 0xc;
    }
    if (strcmp(operand, "di") == 0) {
        return 0xd;
    }
    if (strcmp(operand, "sp") == 0) {
        return 0xe;
    }
    if (strcmp(operand, "bp") == 0) {
        return 0xf;
    }
    return -1;
}

int count_operands(x86_instruction* instruction) {
    int count = 0;
    if (strcmp(instruction->operand1, "") != 0) {
        count++;
    }
    if (strcmp(instruction->operand2, "") != 0) {
        count++;
    }
    if (strcmp(instruction->operand3, "") != 0) {
        count++;
    }
    return count;
}

void add(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "add");
            strcpy(tmp->operand1, "s0");
            strcpy(tmp->operand2, instruction->operand2);
            add(mips_instruction1, tmp, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            free(tmp);
            return;
        }
        tmp = strstr(instruction->operand2, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand2, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            strcpy(tmp->instruction, "add");
            strcpy(tmp->operand2, "s0");
            strcpy(tmp->operand1, instruction->operand1);
            add(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    if (count_operands(instruction) == 3) {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rs = get_register(instruction->operand2);
        mips_instruction1->rt = get_register(instruction->operand3);
        mips_instruction1->instruction = 0x20;
        mips_instruction1->shift_amount = 0x0;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    if (atoi(instruction->operand2) == 0) {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rs = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand2);
        mips_instruction1->instruction = 0x20;
        mips_instruction1->shift_amount = 0x0;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    mips_instruction1->opcode = 0x8;
    mips_instruction1->rt = get_register(instruction->operand1);
    mips_instruction1->rs = get_register(instruction->operand1);
    mips_instruction1->imm = atoi(instruction->operand2);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void AND(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "and");
            strcpy(tmp->operand1, "s0");
            strcpy(tmp->operand2, instruction->operand2);
            AND(mips_instruction1, tmp, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            free(tmp);
            return;
        }
        tmp = strstr(instruction->operand2, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand2, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            strcpy(tmp->instruction, "and");
            strcpy(tmp->operand2, "s0");
            strcpy(tmp->operand1, instruction->operand1);
            AND(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    if (atoi(instruction->operand2) == 0) {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rs = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand2);
        mips_instruction1->instruction = 0x24;
        mips_instruction1->shift_amount = 0x0;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    mips_instruction1->opcode = 0xc;
    mips_instruction1->rt = get_register(instruction->operand1);
    mips_instruction1->rs = get_register(instruction->operand1);
    mips_instruction1->imm = atoi(instruction->operand2);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void dec(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    strcpy(instruction->operand2, "-1");
    add(mips_instruction1, instruction, mips_hex);
}

void inc(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    strcpy(instruction->operand2, "1");
    add(mips_instruction1, instruction,mips_hex);
}

void divide(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "div");
            strcpy(tmp->operand1, "s0");
            divide(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    mips_instruction1->opcode = 0x0;
    mips_instruction1->rs = get_register("eax");
    mips_instruction1->rt = get_register(instruction->operand1);
    mips_instruction1->rd = 0x0;
    mips_instruction1->shift_amount = 0x0;
    mips_instruction1->instruction = 0x1b;
    mips_instruction1->instruction_type = 0;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void idivide(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "idiv");
            strcpy(tmp->operand1, "s0");
            idivide(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    mips_instruction1->opcode = 0x0;
    mips_instruction1->rs = get_register("eax");
    mips_instruction1->rt = get_register(instruction->operand1);
    mips_instruction1->rd = 0x0;
    mips_instruction1->shift_amount = 0x0;
    mips_instruction1->instruction = 0x1a;
    mips_instruction1->instruction_type = 0;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void mul(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "mul");
            strcpy(tmp->operand1, "s0");
            mul(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    mips_instruction1->opcode = 0x0;
    mips_instruction1->rs = get_register("eax");
    mips_instruction1->rt = get_register(instruction->operand1);
    mips_instruction1->rd = 0x0;
    mips_instruction1->shift_amount = 0x0;
    mips_instruction1->instruction = 0x19;
    mips_instruction1->instruction_type = 0;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void imul(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    int count = count_operands(instruction);
    if (count == 1) {
        if (instruction->instruction_type == 3) {
            char* tmp = strstr(instruction->operand1, "[");
            if (tmp != NULL) {
                load_from_mem(instruction->operand1, mips_hex);
                x86_instruction* tmp = malloc(sizeof(x86_instruction));
                tmp->instruction_type = 0;
                strcpy(tmp->instruction, "imul");
                strcpy(tmp->operand1, "s0");
                imul(mips_instruction1, tmp, mips_hex);
                free(tmp);
                return;
            }
        }
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rs = get_register("eax");
        mips_instruction1->rt = get_register(instruction->operand1);
        mips_instruction1->rd = 0x0;
        mips_instruction1->shift_amount = 0x0;
        mips_instruction1->instruction = 0x18;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    if (count == 2) {
        if (instruction->instruction_type == 3) {
            char* tmp = strstr(instruction->operand2, "[");
            if (tmp != NULL) {
                load_from_mem(instruction->operand2, mips_hex);
                x86_instruction* tmp = malloc(sizeof(x86_instruction));
                tmp->instruction_type = 0;
                strcpy(tmp->instruction, "imul");
                strcpy(tmp->operand1, instruction->operand1);
                strcpy(tmp->operand2, "s0");
                imul(mips_instruction1, tmp, mips_hex);
                free(tmp);
                return;
            }
        }
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rs = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand2);
        mips_instruction1->rd = 0x0;
        mips_instruction1->shift_amount = 0x0;
        mips_instruction1->instruction = 0x18;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    if (count == 3) {
        if (instruction->instruction_type == 3) {
            char* tmp = strstr(instruction->operand2, "[");
            if (tmp != NULL) {
                load_from_mem(instruction->operand2, mips_hex);
                x86_instruction* tmp = malloc(sizeof(x86_instruction));
                tmp->instruction_type = 0;
                strcpy(tmp->instruction, "add");
                strcpy(tmp->operand1, "s1");
                strcpy(tmp->operand2, instruction->operand3);
                add(mips_instruction1, tmp, mips_hex);
                strcpy(tmp->instruction, "imul");
                strcpy(tmp->operand1, instruction->operand1);
                strcpy(tmp->operand2, "s0");
                strcpy(tmp->operand3, "s1");
                imul(mips_instruction1, tmp, mips_hex);
                free(tmp);
                return;
            }
        }
        mips_instruction1->opcode = 0x1c;
        mips_instruction1->rs = get_register(instruction->operand2);
        mips_instruction1->rt = get_register(instruction->operand3);
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->shift_amount = 0x0;
        mips_instruction1->instruction = 0x6;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
}

void neg(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand2, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "sub");
            strcpy(tmp->operand1, "s0");
            neg(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    mips_instruction1->opcode = 0x0;
    mips_instruction1->rd = get_register(instruction->operand1);
    mips_instruction1->rs = 0;
    mips_instruction1->rt = get_register(instruction->operand1);
    mips_instruction1->shift_amount = 0x0;
    mips_instruction1->instruction = 0x22;
    mips_instruction1->instruction_type = 0;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void INT(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    x86_instruction* tmp = malloc(sizeof(x86_instruction));
    tmp->instruction_type = 0;
    strcpy(tmp->instruction, "add");
    strcpy(tmp->operand1, "a0");
    strcpy(tmp->operand2, instruction->operand1);
    add(mips_instruction1, tmp, mips_hex);
    strcpy(tmp->instruction, "add");
    strcpy(tmp->operand1, "v0");
    strcpy(tmp->operand2, "17");
    add(mips_instruction1, tmp, mips_hex);
    mips_instruction1->opcode = 63;
    mips_instruction1->imm = 67108863;
    mips_instruction1->instruction_type = 2;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void jump(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "jr");
            strcpy(tmp->operand1, "s0");
            jump_register(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    if (atoi(instruction->operand1) == 0) {
        jump_register(mips_instruction1, instruction, mips_hex);
        return;
    }
    mips_instruction1->opcode = 0x2;
    mips_instruction1->imm = atoi(instruction->operand1);
    mips_instruction1->instruction_type = 2;
    write_hex(mips_instruction1, mips_hex);
    line_count++;
    return;
}

void jump_register(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x0;
    mips_instruction1->rs = get_register(instruction->operand1);
    mips_instruction1->rd = 0x0;
    mips_instruction1->rt = 0x0;
    mips_instruction1->shift_amount = 0x0;
    mips_instruction1->instruction = 0x8;
    mips_instruction1->instruction_type = 0;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void lea(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    strcpy(instruction->operand2, itoa(get_label(instruction->operand1)));
    add(mips_instruction1, instruction, mips_hex);
    return;
}

void loop(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    x86_instruction* tmp = malloc(sizeof(x86_instruction));
    tmp->instruction_type = 0;
    strcpy(tmp->instruction, "dec");
    strcpy(tmp->operand1, "ecx");
    dec(mips_instruction1, tmp, mips_hex);
    mips_instruction1->opcode = 0x5;
    mips_instruction1->rs = get_register("ecx");
    mips_instruction1->rt = get_register("zero");
    mips_instruction1->imm = get_label(instruction->operand1);
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void mov(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "add");
            strcpy(tmp->operand1, "s0");
            strcpy(tmp->operand2, instruction->operand2);
            strcpy(tmp->operand3, "zero");
            add(mips_instruction1, tmp, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            free(tmp);
            return;
        }
        tmp = strstr(instruction->operand2, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "add");
            strcpy(tmp->operand1, instruction->operand1);
            strcpy(tmp->operand2, "s0");
            strcpy(tmp->operand3, "zero");
            add(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    strcpy(instruction->operand3, "zero");
    add(mips_instruction1, instruction, mips_hex);
    return;
}

void nop(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    strcpy(instruction->operand1, "zero");
    strcpy(instruction->operand2, "zero");
    strcpy(instruction->operand3, "zero");
    add(mips_instruction1, instruction, mips_hex);
}

void NOT(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            mips_instruction1->opcode = 0x0;
            mips_instruction1->rd = get_register("s0");
            mips_instruction1->rs = get_register("s0");
            mips_instruction1->rt = 0x0;
            mips_instruction1->shift_amount = 0x0;
            mips_instruction1->instruction = 0x5;
            mips_instruction1->instruction_type = 0;
            line_count++;
            write_hex(mips_instruction1, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            return;
        }
    }
    mips_instruction1->opcode = 0x0;
    mips_instruction1->rd = get_register(instruction->operand1);
    mips_instruction1->rs = get_register(instruction->operand1);
    mips_instruction1->rt = 0x0;
    mips_instruction1->shift_amount = 0x0;
    mips_instruction1->instruction = 0x5;
    mips_instruction1->instruction_type = 0;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void OR(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "or");
            strcpy(tmp->operand1, "s0");
            strcpy(tmp->operand2, instruction->operand2);
            OR(mips_instruction1, tmp, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            free(tmp);
            return;
        }
        tmp = strstr(instruction->operand2, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand2, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "or");
            strcpy(tmp->operand2, "s0");
            strcpy(tmp->operand1, instruction->operand1);
            OR(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    if (atoi(instruction->operand2) == 0) {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rs = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand2);
        mips_instruction1->instruction = 0x25;
        mips_instruction1->shift_amount = 0x0;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    mips_instruction1->opcode = 0xd;
    mips_instruction1->rt = get_register(instruction->operand1);
    mips_instruction1->rs = get_register(instruction->operand1);
    mips_instruction1->imm = atoi(instruction->operand2);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void pop(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem("esp", mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            stpcpy(tmp->instruction, "mov");
            strcpy(tmp->operand1, instruction->operand1);
            strcpy(tmp->operand2, "s0");
            mov(mips_instruction1, tmp, mips_hex);
            strcpy(instruction->operand1, "esp");
            dec(mips_instruction1, instruction, mips_hex);
            free(tmp);
            return;
        }
    }
    if (strcmp(instruction->operand1, NULL) == 0) {
        strcpy(instruction->operand1, "esp");
        dec(mips_instruction1, instruction, mips_hex);
        return;
    } else {
        load_from_mem("esp", mips_hex);
        x86_instruction* tmp = malloc(sizeof(x86_instruction));
        tmp->instruction_type = 0;
        stpcpy(tmp->instruction, "mov");
        strcpy(tmp->operand1, instruction->operand1);
        strcpy(tmp->operand2, "s0");
        mov(mips_instruction1, tmp, mips_hex);
        strcpy(instruction->operand1, "esp");
        dec(mips_instruction1, instruction, mips_hex);
        free(tmp);
        return;
    }
}

void push(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            stpcpy(tmp->instruction, "mov");
            strcpy(tmp->operand1, "esp");
            strcpy(tmp->operand2, "s0");
            mov(mips_instruction1, tmp, mips_hex);
            strcpy(instruction->operand1, "esp");
            inc(mips_instruction1, instruction, mips_hex);
            free(tmp);
            return;
        }
    }
    strcpy(instruction->operand2, instruction->operand1);
    strcpy(instruction->operand1, "esp");
    mov(mips_instruction1, instruction, mips_hex);
    inc(mips_instruction1, instruction, mips_hex);
    return;
}

void ret(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (strcmp(instruction->operand1, NULL) != 0) {
        load_from_mem("esp", mips_hex);
        strcpy(instruction->instruction, "jr");
        strcpy(instruction->operand1, "s0");
        jump_register(mips_instruction1, instruction, mips_hex);
        
    }
    load_from_mem("esp", mips_hex);
    strcpy(instruction->instruction, "jr");
    strcpy(instruction->operand1, "s0");
    jump_register(mips_instruction1, instruction, mips_hex);
    char* tmp = malloc(sizeof(instruction->operand1));
    strcpy(tmp, "-");
    strcat(tmp, instruction->operand1);
    strcpy(instruction->operand1, "esp");
    strcpy(instruction->operand2, tmp);
    add(mips_instruction1, instruction, mips_hex);
    return;
}

void sal(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            stpcpy(tmp->instruction, "sal");
            strcpy(tmp->operand1, instruction->operand1);
            strcpy(tmp->operand2, "s0");
            sal(mips_instruction1, tmp, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            free(tmp);
            return;
        }
    }
    if (count_operands(instruction) == 2) {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand1);
        if (atoi(instruction->operand2) == 0) {
            mips_instruction1->shift_amount = get_register(instruction->operand2);
        } else {
            mips_instruction1->shift_amount = atoi(instruction->operand2);
        }
        mips_instruction1->rs = 0x0;
        mips_instruction1->instruction = 0x0;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    else {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand1);
        mips_instruction1->shift_amount = 0x1;
        mips_instruction1->instruction = 0x0;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
}

void sar(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            stpcpy(tmp->instruction, "sar");
            strcpy(tmp->operand1, instruction->operand1);
            strcpy(tmp->operand2, "s0");
            sar(mips_instruction1, tmp, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            free(tmp);
            return;
        }
    }
    if (count_operands(instruction) == 2) {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand1);
        if (atoi(instruction->operand2) == 0) {
            mips_instruction1->shift_amount = get_register(instruction->operand2);
        } else {
            mips_instruction1->shift_amount = atoi(instruction->operand2);
        }
        mips_instruction1->rs = 0x0;
        mips_instruction1->instruction = 0x3;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    else {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand1);
        mips_instruction1->shift_amount = 0x1;
        mips_instruction1->instruction = 0x3;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
}

void shl(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    sal(mips_instruction1, instruction, mips_hex);
}

void shr(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            stpcpy(tmp->instruction, "shr");
            strcpy(tmp->operand1, instruction->operand1);
            strcpy(tmp->operand2, "s0");
            shr(mips_instruction1, tmp, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            free(tmp);
            return;
        }
    }
    if (count_operands(instruction) == 2) {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand1);
        if (atoi(instruction->operand2) == 0) {
            mips_instruction1->shift_amount = get_register(instruction->operand2);
        } else {
            mips_instruction1->shift_amount = atoi(instruction->operand2);
        }
        mips_instruction1->rs = 0x0;
        mips_instruction1->instruction = 0x2;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    else {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand1);
        mips_instruction1->shift_amount = 0x1;
        mips_instruction1->instruction = 0x2;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
}

void test(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
   
}

void xchg(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            stpcpy(tmp->instruction, "mov");
            strcpy(tmp->operand1, "s1");
            strcpy(tmp->operand2, "s0");
            mov(mips_instruction1, tmp, mips_hex);
            mov(mips_instruction1, instruction, mips_hex);
            strcpy(instruction->operand1, instruction->operand2);
            strcpy(instruction->operand2, "s1");
            mov(mips_instruction1, instruction, mips_hex);
            free(tmp);
            return;
        }
        tmp = strstr(instruction->operand2, "[");
        if (tmp != NULL) {
            tmp = malloc(sizeof(instruction->operand1));
            strcpy(tmp, instruction->operand1);
            strcpy(instruction->operand1, instruction->operand2);
            strcpy(instruction->operand2, tmp);
            xchg(mips_instruction1, instruction, mips_hex);
        }
    }
    x86_instruction* tmp = malloc(sizeof(x86_instruction));
    tmp->instruction_type = 0;
    stpcpy(tmp->instruction, "mov");
    strcpy(tmp->operand1, "s0");
    strcpy(tmp->operand2, instruction->operand1);
    mov(mips_instruction1, tmp, mips_hex);
    mov(mips_instruction1, instruction, mips_hex);
    strcpy(tmp->operand1, instruction->operand2);
    strcpy(tmp->operand2, "s0");
    mov(mips_instruction1, tmp, mips_hex);
    free(tmp);
    return;
}

void xlat(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    load_from_mem("eax", mips_hex);
    strcpy(instruction->instruction, "mov");
    strcpy(instruction->operand1, "eax");
    strcpy(instruction->operand2, "s0");
    mov(mips_instruction1, instruction, mips_hex);
    return;
}

void XOR(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "xor");
            strcpy(tmp->operand1, "s0");
            strcpy(tmp->operand2, instruction->operand2);
            XOR(mips_instruction1, tmp, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            free(tmp);
            return;
        }
        tmp = strstr(instruction->operand2, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand2, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            strcpy(tmp->instruction, "xor");
            strcpy(tmp->operand2, "s0");
            strcpy(tmp->operand1, instruction->operand1);
            XOR(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    if (atoi(instruction->operand2) == 0) {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = get_register(instruction->operand1);
        mips_instruction1->rs = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand2);
        mips_instruction1->instruction = 0x26;
        mips_instruction1->shift_amount = 0x0;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    mips_instruction1->opcode = 0xe;
    mips_instruction1->rt = get_register(instruction->operand1);
    mips_instruction1->rs = get_register(instruction->operand1);
    mips_instruction1->imm = atoi(instruction->operand2);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void call(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    x86_instruction* tmp = malloc(sizeof(x86_instruction));
    tmp->instruction_type = 0;
    strcpy(instruction->instruction, "pop");
    int address = line_count + 1;
    sprintf(tmp->operand1, "%d", address);
    pop(mips_instruction1, tmp, mips_hex);
    jump(mips_instruction1, instruction, mips_hex);
    free(tmp);
    return;
}

void cmp(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 3) {
        char* tmp = strstr(instruction->operand1, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand1, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            tmp->instruction_type = 0;
            strcpy(tmp->instruction, "cmp");
            strcpy(tmp->operand1, "s0");
            strcpy(tmp->operand2, instruction->operand2);
            cmp(mips_instruction1, tmp, mips_hex);
            store_to_mem(instruction->operand1, mips_hex);
            free(tmp);
            return;
        }
        tmp = strstr(instruction->operand2, "[");
        if (tmp != NULL) {
            load_from_mem(instruction->operand2, mips_hex);
            x86_instruction* tmp = malloc(sizeof(x86_instruction));
            strcpy(tmp->instruction, "xor");
            strcpy(tmp->operand2, "s0");
            strcpy(tmp->operand1, instruction->operand1);
            cmp(mips_instruction1, tmp, mips_hex);
            free(tmp);
            return;
        }
    }
    if (atoi(instruction->operand2) == 0) {
        mips_instruction1->opcode = 0x0;
        mips_instruction1->rd = 0x0;
        mips_instruction1->rs = get_register(instruction->operand1);
        mips_instruction1->rt = get_register(instruction->operand2);
        mips_instruction1->instruction = 0xe;
        mips_instruction1->shift_amount = 0x0;
        mips_instruction1->instruction_type = 0;
        line_count++;
        write_hex(mips_instruction1, mips_hex);
        return;
    }
    mips_instruction1->opcode = 0x13;
    mips_instruction1->rt = 0x0;
    mips_instruction1->rs = get_register(instruction->operand1);
    mips_instruction1->imm = atoi(instruction->operand2);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void ja(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x5;
    mips_instruction1->rs = get_register("s4");
    mips_instruction1->rt = get_register("zero");
    mips_instruction1->imm = get_label(instruction->operand1);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void jae(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x5;
    mips_instruction1->rs = get_register("s6");
    mips_instruction1->rt = get_register("zero");
    mips_instruction1->imm = get_label(instruction->operand1);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void jb(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x5;
    mips_instruction1->rs = get_register("s2");
    mips_instruction1->rt = get_register("zero");
    mips_instruction1->imm = get_label(instruction->operand1);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void jbe(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x5;
    mips_instruction1->rs = get_register("s5");
    mips_instruction1->rt = get_register("zero");
    mips_instruction1->imm = get_label(instruction->operand1);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void jcxz(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x4;
    mips_instruction1->rs = get_register("cx");
    mips_instruction1->rt = get_register("zero");
    mips_instruction1->imm = get_label(instruction->operand1);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
}

void jecxz(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x4;
    mips_instruction1->rs = get_register("ecx");
    mips_instruction1->rt = get_register("zero");
    mips_instruction1->imm = get_label(instruction->operand1);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
}

void je(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x5;
    mips_instruction1->rs = get_register("s3");
    mips_instruction1->rt = get_register("zero");
    mips_instruction1->imm = get_label(instruction->operand1);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
}

void jg(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    ja(mips_instruction1, instruction, mips_hex);
}

void jge(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jae(mips_instruction1, instruction, mips_hex);
}

void jl(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jb(mips_instruction1, instruction, mips_hex);
}

void jle(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jbe(mips_instruction1, instruction, mips_hex);
}

void jna(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jle(mips_instruction1, instruction, mips_hex);
}

void jnae(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jle(mips_instruction1, instruction, mips_hex);
}

void jnb(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jge(mips_instruction1, instruction, mips_hex);
}

void jnbe(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jge(mips_instruction1, instruction, mips_hex);
}

void jne(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x5;
    mips_instruction1->rs = get_register("s7");
    mips_instruction1->rt = get_register("zero");
    mips_instruction1->imm = get_label(instruction->operand1);
    mips_instruction1->instruction_type = 1;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
}

void jng(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jna(mips_instruction1, instruction, mips_hex);
}

void jnge(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jnae(mips_instruction1, instruction, mips_hex);
}

void jnl(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jnb(mips_instruction1, instruction, mips_hex);
}

void jnle(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jnbe(mips_instruction1, instruction, mips_hex);
}

void jnz(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    jne(mips_instruction1, instruction, mips_hex);
}

void jz(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    je(mips_instruction1, instruction, mips_hex);
}

void bswap(mips_instruction* mips_instruction1, x86_instruction* instruction, FILE* mips_hex) {
    mips_instruction1->opcode = 0x0;
    mips_instruction1->rd = get_register(instruction->operand1);
    mips_instruction1->rs = get_register(instruction->operand1);
    mips_instruction1->instruction = 0x14;
    mips_instruction1->shift_amount = 0x0;
    mips_instruction1->instruction_type = 0;
    line_count++;
    write_hex(mips_instruction1, mips_hex);
    return;
}

void load_from_mem(char* label, FILE* mips_hex) {
    mips_instruction* instruction = malloc(sizeof(mips_instruction));
    instruction->opcode = 0x23;
    instruction->rt = TMP_REGISTER;
    instruction->imm = get_label(label);
    instruction->instruction_type = 1;
    line_count++;
    write_hex(instruction, mips_hex);
}

void store_to_mem(char* label, FILE* mips_hex) {
    mips_instruction* instruction = malloc(sizeof(mips_instruction));
    instruction->opcode = 0x2b;
    instruction->rt = TMP_REGISTER;
    instruction->imm = get_label(label);
    instruction->instruction_type = 1;
    line_count++;
    write_hex(instruction, mips_hex);
}

void write_hex(mips_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 0) {
        int output = 0x00000000;
        output |= (instruction->opcode << 26);
        output |= (instruction->rs << 21);
        output |= (instruction->rt << 16);
        output |= (instruction->rd << 11);
        output |= (instruction->instruction);
        fprintf(mips_hex, "%.8x", output);
        if (instruction->next != NULL) {
            write_hex(instruction->next, mips_hex);
        }
        else {
            return;
        }
    }
    if (instruction->instruction_type == 1) {
        int output = 0x00000000;
        output |= (instruction->opcode << 26);
        output |= (instruction->rs << 21);
        output |= (instruction->rt << 16);
        output |= (instruction->imm);
        fprintf(mips_hex, "%.8x", output);
        if (instruction->next != NULL) {
            write_hex(instruction->next, mips_hex);
        }
        else {
            return;
        }
    }
    if (instruction->instruction_type == 2) {
        int output = 0x00000000;
        output |= (instruction->opcode << 26);
        output |= (instruction->address);
        fprintf(mips_hex, "%.8x", output);
        if (instruction->next != NULL) {
            write_hex(instruction->next, mips_hex);
        }
        else {
            return;
        }
    }
}

void load_mips_instruction(x86_instruction* instruction, FILE* mips_hex) {
    if (instruction->instruction_type == 2 || instruction->instruction_type == 1) {
        return;
    }
    mips_instruction* mips_instruction1 = malloc(sizeof(mips_instruction));
    if (strcmp(instruction->instruction, "add") == 0) {
        add(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "and") == 0) {
        AND(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "dec") == 0) {
        dec(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "inc") == 0) {
        inc(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "div") == 0) {
        divide(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "idiv") == 0) {
        idivide(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "mul") == 0) {
        mul(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "imul") == 0) {
        imul(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "neg") == 0) {
        neg(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "int") == 0) {
        INT(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jmp") == 0) {
        jump(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "lea") == 0) {
        lea(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "loop") == 0) {
        loop(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "mov") == 0) {
        mov(mips_instruction1, instruction, mips_hex);
    }
    if (strcmp(instruction->instruction, "nop") == 0) {
        nop(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "not") == 0) {
        NOT(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "or") == 0) {
        OR(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "pop") == 0) {
        pop(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "push") == 0) {
        push(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "ret") == 0) {
        ret(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "sal") == 0) {
        sal(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "sar") == 0) {
        sar(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "shl") == 0) {
        shl(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "shr") == 0) {
        shr(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "test") == 0) {
        test(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "xchg") == 0) {
        xchg(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "xlat") == 0) {
        xlat(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "xor") == 0) {
        XOR(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "call") == 0) {
        call(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "cmp") == 0) {
        cmp(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "ja") == 0) {
        ja(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jb") == 0) {
        jb(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jae") == 0) {
        jae(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jb") == 0) {
        jb(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jbe") == 0) {
        jbe(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jcxz") == 0) {
        jcxz(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jecxz") == 0) {
        jecxz(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "je") == 0) {
        je(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jg") == 0) {
        jg(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jge") == 0) {
        jge(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jl") == 0) {
        jl(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jle") == 0) {
        jle(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jna") == 0) {
        jna(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jnae") == 0) {
        jnae(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jnb") == 0) {
        jnb(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jnbe") == 0) {
        jnbe(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jne") == 0) {
        jne(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jng") == 0) {
        jng(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jnge") == 0) {
        jnge(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jnl") == 0) {
        jnl(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jnle") == 0) {
        jnle(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jnz") == 0) {
        jnz(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "jz") == 0) {
        jz(mips_instruction1, instruction, mips_hex);
        return;
    }
    if (strcmp(instruction->instruction, "bswap") == 0) {
        bswap(mips_instruction1, instruction, mips_hex);
        return;
    }
    printf("Unknown operand @ Line %i", line_count);
    exit(5);
}
