//
//  x86_interpreter.h
//  x86 Final
//
//  Created by Christopher Clarke on 4/4/16.
//  Copyright © 2016 Assembly Language. All rights reserved.
//

#ifndef x86_interpreter_h
#define x86_interpreter_h

#include <stdio.h>

#define TMP_REGISTER 0x10 // $s0 temporary register for storing
#define TMP_REGISTER_2 0x11 // $s1 temporary register for storing
#define LESS_THAN_FLAG 0x12 // $s2 less than flag
#define EQUAL_FLAG 0x13 // $s3 equal flag
#define GREATER_THAN_FLAG 0x14 // $s4 greater than flag
#define LESS_THAN_EQUAL_FLAG 0x15 // $s5 less than or equal flag
#define GREATER_THAN_EQUAL_FLAG 0x16 // 0x16 $s6 greater than or eqaul flag
#define NOT_EQUAL_FLAG 0x17 // 0x16 $s6 greater than or eqaul flag


typedef struct x86_instruction {
    char label[25];
    char instruction[25];
    char operand1[25];
    char operand2[25];
    char operand3[25];
    int instruction_type; // 0 if mnemonic w/o memory, 1 if label, 2 if .data keyword, 3 if mnemonic with memory
} x86_instruction;

/*typedef struct x86_register {
    int32_t eax;
    int32_t ebx;
    int32_t ecx;
    int32_t edx;
    int32_t esp;
    int32_t esi;
    int32_t edi;
    int32_t ebp;
    int16_t ax;
    int16_t bx;
    int16_t cx;
    int16_t dx;
    int16_t sp;
    int16_t si;
    int16_t di;
    int16_t bp;
} x86_register;*/

typedef struct mips_instruction {
    int opcode;
    int rd;
    int rs;
    int rt;
    int imm;
    int address;
    int instruction;
    int shift_amount;
    int instruction_type; // 0 if r type, 1 if i type, 2 if j type
    struct mips_instruction* next;
} mips_instruction;

typedef struct data_instruction {
    char variable_name[20];
    char keyword[10];
    char mem_size[5];
    char initial_value[20];
    int amount;
    int instruction_type; // 0 if DW/DD/DB/DQ, 1 if TIMES
}data_instruction;

typedef struct symbol {
    char name[25];
    int mem_address;
    struct symbol* next;
}symbol;

typedef struct data {
    char name[25];
    int offset;
    struct data* next;
}data;

x86_instruction* decode_instruction(char*);
data_instruction* decode_data_segment(char*);
int get_register(char*);
int get_mem_size(char*);
int get_label(char*);
void add(mips_instruction*, x86_instruction*, FILE*);
void AND(mips_instruction*, x86_instruction*, FILE*);
void divide(mips_instruction*, x86_instruction*, FILE*);
void idivide(mips_instruction*, x86_instruction*, FILE*);
void mul(mips_instruction*, x86_instruction*, FILE*);
void imul(mips_instruction*, x86_instruction*, FILE*);
void neg(mips_instruction*, x86_instruction*, FILE*);
void INT(mips_instruction*, x86_instruction*, FILE*);
void jump(mips_instruction*, x86_instruction*, FILE*);
void jump_register(mips_instruction*, x86_instruction*, FILE*); // helper function
void lea(mips_instruction*, x86_instruction*, FILE*);
void loop(mips_instruction*, x86_instruction*, FILE*);
void mov(mips_instruction*, x86_instruction*, FILE*);
void nop(mips_instruction*, x86_instruction*, FILE*);
void NOT(mips_instruction*, x86_instruction*, FILE*);
void OR(mips_instruction*, x86_instruction*, FILE*);
void pop(mips_instruction*, x86_instruction*, FILE*);
void push(mips_instruction*, x86_instruction*, FILE*);
void ret(mips_instruction*, x86_instruction*, FILE*);
void sal(mips_instruction*, x86_instruction*, FILE*);
void sar(mips_instruction*, x86_instruction*, FILE*);
void shl(mips_instruction*, x86_instruction*, FILE*);
void shr(mips_instruction*, x86_instruction*, FILE*);
void test(mips_instruction*, x86_instruction*, FILE*);
void xchg(mips_instruction*, x86_instruction*, FILE*);
void xlat(mips_instruction*, x86_instruction*, FILE*);
void XOR(mips_instruction*, x86_instruction*, FILE*);
void call(mips_instruction*, x86_instruction*, FILE*);
void cmp(mips_instruction*, x86_instruction*, FILE*);
void ja(mips_instruction*, x86_instruction*, FILE*);
void jae(mips_instruction*, x86_instruction*, FILE*);
void jb(mips_instruction*, x86_instruction*, FILE*);
void jbe(mips_instruction*, x86_instruction*, FILE*);
void jcxz(mips_instruction*, x86_instruction*, FILE*);
void jecxz(mips_instruction*, x86_instruction*, FILE*);
void je(mips_instruction*, x86_instruction*, FILE*);
void jg(mips_instruction*, x86_instruction*, FILE*);
void jge(mips_instruction*, x86_instruction*, FILE*);
void jl(mips_instruction*, x86_instruction*, FILE*);
void jle(mips_instruction*, x86_instruction*, FILE*);
void jna(mips_instruction*, x86_instruction*, FILE*);
void jnae(mips_instruction*, x86_instruction*, FILE*);
void jnb(mips_instruction*, x86_instruction*, FILE*);
void jnbe(mips_instruction*, x86_instruction*, FILE*);
void jne(mips_instruction*, x86_instruction*, FILE*);
void jng(mips_instruction*, x86_instruction*, FILE*);
void jnge(mips_instruction*, x86_instruction*, FILE*);
void jnl(mips_instruction*, x86_instruction*, FILE*);
void jnle(mips_instruction*, x86_instruction*, FILE*);
void jnz(mips_instruction*, x86_instruction*, FILE*);
void jz(mips_instruction*, x86_instruction*, FILE*);
void bswap(mips_instruction*, x86_instruction*, FILE*);
void load_from_mem(char*, FILE*);
void store_to_mem(char*, FILE*);
void load_mips_instruction(x86_instruction*, FILE*);
void load_symbol_table(x86_instruction*);
void load_data_symbol(data*);
void load_data_segment(data_instruction*);
void load_symbol(char*);
void write_hex(mips_instruction*, FILE*);
int get_label_address(char*);
int hashfunction(char*);
int count_operands(x86_instruction*);
char* itoa(int);

#endif /* x86_interpreter_h */
